import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { DefaultTextPipe } from './pipes/default-text.pipe';
import { TasksTabComponent } from './tasks-tab/tasks-tab.component';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { MainService } from './services/main.service';
import { HttpClientModule } from '@angular/common/http';
import { TaskListComponent } from './task-list/task-list.component';
import { ProjectCreatorComponent } from './project-creator/project-creator.component';
import { FormsModule } from '@angular/forms';
import { TaskAddDirective } from './directives/task-add.directive';
import { TaskManageDirective } from './directives/task-manage.directive';
import { TaskDeleteDirective } from './directives/task-delete.directive';

@NgModule({
  declarations: [
    AppComponent,
    DefaultTextPipe,
    TasksTabComponent,
    LeftMenuComponent,
    TaskListComponent,
    ProjectCreatorComponent,
    TaskAddDirective,
    TaskManageDirective,
    TaskDeleteDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [MainService],
  bootstrap: [AppComponent]
})
export class AppModule { }
