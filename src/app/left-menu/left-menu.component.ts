import { Component, Input, Output } from '@angular/core';
import { Project } from '../libs/project.interface';
import { MainService } from '../services/main.service';
import { EventEmitter } from 'events';
import { Menu } from '../libs/menu.enum';
import { ProjectCreator } from '../libs/projectCreator.enum';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.css']
})
export class LeftMenuComponent {

  menuClass = 'wrapper ext';
  projectsList: Array<Project> = null;

  constructor(private main: MainService) {
    this.main.getProjects().subscribe(data => {
      if(this.projectsList == null && data != null) {
        main.changeProject(data[0]);
      }
      this.projectsList = data;
    });
    this.main.getMenu().subscribe(data => {
      this.menuClass = data;
    });
  }

  hideMenu() {
    this.main.menu(Menu.HIDE);
  }

  changeProject(project: Project) {
    this.hideMenu();
    this.main.changeProject(project);
  }
  newProject() {
    this.main.projectCreator(ProjectCreator.SHOW);
    this.main.menu(Menu.HIDE);
  }

}
