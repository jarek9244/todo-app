import { Directive, ElementRef, Renderer2, HostListener, Input } from '@angular/core';
import { MainService } from '../services/main.service';
import { Task } from '../libs/task.interface';

@Directive({
  selector: '[appTaskDelete]'
})
export class TaskDeleteDirective {

  @Input()
  task: Task;
  @Input()
  index: number;
  private btn;

  constructor(private el: ElementRef, private renderer: Renderer2, private main: MainService) {
    this.btn = this.renderer.createElement('p');
    this.btn.setAttribute('class', 'delete');
   }

  @HostListener('mouseenter')
  mouseEnter() {
    this.btn.innerHTML = 'x';
    this.renderer.appendChild(this.el.nativeElement, this.btn);
    this.btn.addEventListener('click', v => {
      this.main.deleteTask(this.task.ID, this.task.TaskID, this.index);
    });
  }

  @HostListener('mouseleave')
  mouseLeave() {
    this.renderer.removeChild(this.el.nativeElement, this.btn);
  }
}
