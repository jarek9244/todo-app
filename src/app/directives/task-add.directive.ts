import { Directive, ElementRef, Renderer2, OnInit, HostListener, Input } from '@angular/core';
import { MainService } from '../services/main.service';

@Directive({
  selector: '[appTaskAdd]'
})
export class TaskAddDirective {

  @Input()
  info: Array<number>;

  constructor(private el: ElementRef, private main: MainService) {

  }

  @HostListener('change')
  addTask() {
    const projectID = this.info[0];
    const taskID = this.info[1];
    const value = this.el.nativeElement.value;

    if(value !== '' && value !== null) {
      this.main.createTask(projectID, taskID, value);
      this.el.nativeElement.value = '';
    }
  }
}
