export enum API {
  PROJECTS = 'http://192.168.1.8/api/Angular/todo-app/loadProjects.php',
  TASKS = 'http://192.168.1.8/api/Angular/todo-app/loadTasks.php/',
  SUBMIT = 'http://192.168.1.8/api/Angular/todo-app/submitTask.php',
  NEW_PROJECT = 'http://192.168.1.8/api/Angular/todo-app/newProject.php',
  RENAME = 'http://192.168.1.8/api/Angular/todo-app/renameTask.php',
  CREATE = 'http://192.168.1.8/api/Angular/todo-app/newTask.php',
  DELETE = 'http://192.168.1.8/api/Angular/todo-app/deleteTask.php'
}
