export interface Project {
  ID: number;
  Title: string;
  Description: string;
  Created: string;
  Ended: string;
}
