export interface Task {
  ID: number;
  ProjectID: number;
  TaskID: number;
  Title: string;
  Created: string;
  Done: number;
  Subtasks?: Array<Task>;
}
