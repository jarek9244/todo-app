import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Project } from '../libs/project.interface';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { API } from '../libs/api.config';
import { Subject } from 'rxjs/Subject';
import { Menu } from '../libs/menu.enum';
import { Task } from '../libs/task.interface';
import { Info } from '../libs/info.interface';

@Injectable()
export class MainService {

  projectsList: Array<Project> = null;
  currentProject: Project = null;
  tasks: Array<Task> = null;
  canLoad = true;
  progress = null;
  CreatorP = null;


  $projects: Subject<Array<Project>> = new BehaviorSubject<Array<Project>>(null);
  $project: Subject<Project> = new BehaviorSubject<Project>(null);
  $menu: Subject<string> = new BehaviorSubject<string>(Menu.HIDE);
  $tasks: Subject<Array<Task>> = new BehaviorSubject<Array<Task>>(null);
  $status: Subject<number> = new BehaviorSubject<number>(0);
  $projectCreator: Subject<string> = new BehaviorSubject<string>(null);

  constructor(private $http: HttpClient) {
    this.loadProjects().subscribe(data => {
      this.projectsList = data;
      this.$projects.next(this.projectsList);
    });
  }

  public getProjects(): Observable<Array<Project>> {
    return this.$projects;
  }

  public getCurrentProject(): Observable<Project> {
    return this.$project;
  }

  public changeProject(project: Project) {
    if(this.canLoad && this.projectsList.length) {
      this.canLoad = false;
      this.progress = 0;
      this.$status.next(this.progress);
      this.currentProject = project;
      this.$project.next(this.currentProject);
      this.tasks = null;
      this.$tasks.next(this.tasks);
      this.loadTaskObs(this.currentProject.ID);
    } else {
      this.$tasks.next([]);
    }
  }

  public menu(state: string) {
    this.$menu.next(state);
  }

  public getMenu(): Observable<string> {
    return this.$menu;
  }

  public getTasks(): Observable<Array<Task>> {
    return this.$tasks;
  }

  private loadProjects(): Observable<Array<Project>> {
    return this.$http.get<Array<Project>>(API.PROJECTS);
  }

  private loadTaskObs(id: number) {
    this.loadTasks(id).subscribe(data => {
      this.tasks = data;
      this.$tasks.next(this.tasks);
      this.canLoad = true;

      let tempNums = 0;
      let tempCheck = 0;

      for(const index in data) {
        if (data.hasOwnProperty(index)) {
          tempNums++;
          tempCheck += data[index].Done;
          for(const subTask of data[index]['Subtasks']) {
            tempNums++;
            tempCheck += subTask.Done;
          }
        }
      }
      this.progress = Math.round((tempCheck/tempNums) * 100);
      this.$status.next(this.progress);
    });
  }

  private loadTasks(id: number): Observable<Array<Task>> {
    return this.$http.get<Array<Task>>(API.TASKS + id);
  }

  public submitTask(id: number): Observable<Info> {
    return this.$http.post<Info>(API.SUBMIT, {'id': id}, {
      headers : {
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    });
  }

  public setStatus(val: number) {
    this.progress = val;
    this.$status.next(this.progress);
  }

  public getStatus(): Observable<number> {
    return this.$status;
  }

  public projectCreator(val: string) {
    this.CreatorP = val;
    this.$projectCreator.next(this.CreatorP);
  }

  public syncProjectCreator(): Observable<string> {
    return this.$projectCreator;
  }

  public createProject(title: string, desc: string): Observable<Array<number>> {
    return this.$http.post<Array<number>>(API.NEW_PROJECT, {'Title': title, 'Desc': desc}, {
      headers : {
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }});
  }

  public renameTask(id: number, text: string): Observable<Array<number>> {
    return this.$http.post<Array<number>>(API.RENAME, {'ID': id, 'data': text}, {
      headers : {
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }});
  }

  public createTask(projectID: number, taskID: number, text: string) {
    this.$crateTask(projectID, taskID, text).subscribe(data => {
      if(data['task'].TaskID === 0) {
        this.tasks[data['task'].ID] = data['task'];
        this.tasks[data['task'].ID]['Subtasks'] = [];
      } else {
        this.tasks[taskID]['Subtasks'].push(data['task']);
      }
      this.$tasks.next(this.tasks);
    });
  }

  private $crateTask(projectID: number, taskID: number, text: string): Observable<Array<number|Task>> {
    return this.$http.post<Array<number|Task>>(API.CREATE, {'PID': projectID, 'TID': taskID, 'data': text}, {
      headers : {
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }});
  }

  deleteTask(taskID: number, mainTask: number, index?: number) {
    console.log('click');

    this.$deleteTask(taskID).subscribe(data => {
      if(mainTask !== 0) {
        this.tasks[mainTask]['Subtasks'].splice(index, 1);
        this.$tasks.next(this.tasks);
      } else {
        delete this.tasks[taskID];
        this.$tasks.next(this.tasks);
      }
    });
  }

  $deleteTask(taskID: number): Observable<number> {
    return this.$http.post<number>(API.DELETE, {'ID': taskID}, {
      headers : {
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
    }});
  }
}
