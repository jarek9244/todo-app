import { Component, OnInit } from '@angular/core';
import { MainService } from '../services/main.service';
import { Task } from '../libs/task.interface';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent {

  tasksList: Array<Task> = null;
  perc: number = null;

  constructor(private main: MainService) {
    this.main.getTasks().subscribe(data => {
      if(data != null ) {
        const temp: Array<Task> = [];
        for(const index in data) {
          if (data.hasOwnProperty(index)) {
            temp.push(data[index]);
          }
        }
        this.tasksList = temp;
      }
    });

  }

  submitTask(id: number) {
    this.main.submitTask(id).subscribe(data => {
      if(data.code === 0) {
        this.main.setStatus(data.perc);
        document.querySelector('#t' + id).setAttribute('class', 'state s' + data.value);
      }
    });
  }

  renameTask(id: number) {
    const obj = <HTMLInputElement> document.querySelector('#i' + id);
    const data = obj.value;

    this.main.renameTask(id, data).subscribe(e => {});
  }
}
