import { Component, OnInit, Input } from '@angular/core';
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-tasks-tab',
  templateUrl: './tasks-tab.component.html',
  styleUrls: ['./tasks-tab.component.css']
})
export class TasksTabComponent {

  @Input()
  projectID: number;

  constructor(private main: MainService) { }

  addTask() {
    if(this.projectID !== null) {
      const val = <HTMLInputElement> document.querySelector('#newTask');
      if(val.value !== null && val.value !== '') {
        this.main.createTask(this.projectID, 0, val.value);
        val.value = '';
      }
    }
  }
}
