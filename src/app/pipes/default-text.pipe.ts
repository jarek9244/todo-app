import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'defaultText'
})
export class DefaultTextPipe implements PipeTransform {

  transform(value: string, args: string): string {
    if(value === null) {
      value = args;
    }
    return value;
  }
}
