import { Component } from '@angular/core';
import { Task } from './libs/task.interface';
import { Project } from './libs/project.interface';
import { MainService } from './services/main.service';
import { Menu } from './libs/menu.enum';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  currentProject: Project = null;
  currentTab = 1;
  perc = 0;

  projectClass = 'projectCreator';

  constructor(private main: MainService) {
    this.main.getCurrentProject().subscribe(data => {
      this.currentProject = data;
    });
    this.main.getStatus().subscribe(data => {
      this.perc = data;
    });
    this.main.syncProjectCreator().subscribe(data => {
      if(data !== null) {
        this.projectClass = data;
        if(data === 'projectCreator') {
          $('#bg').fadeOut(300);
        } else {
          $('#bg').fadeIn(300);
        }
      }
    });
  }

  showMenu() {
    this.main.menu(Menu.SHOW);
  }
}
