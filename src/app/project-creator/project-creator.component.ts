import { Component } from '@angular/core';
import { MainService } from '../services/main.service';
import { ProjectCreator } from '../libs/projectCreator.enum';

@Component({
  selector: 'app-project-creator',
  templateUrl: './project-creator.component.html',
  styleUrls: ['./project-creator.component.css']
})
export class ProjectCreatorComponent {

  title: string;
  desc: string;

  constructor(private main: MainService) {

  }

  hide() {
    this.main.projectCreator(ProjectCreator.HIDE);
    this.title = '';
    this.desc = '';
  }

  addProject() {
    this.main.createProject(this.title, this.desc).subscribe(data => {
      if(data['code'] === 0) {
        // Dodano projekt
        this.main.projectCreator(ProjectCreator.HIDE);
        this.title = '';
        this.desc = '';
      }
    });
  }

}
